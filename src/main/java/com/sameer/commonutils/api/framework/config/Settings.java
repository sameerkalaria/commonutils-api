package com.sameer.commonutils.api.framework.config;

/*
 * This class is for store all the properties value into variable. So, it will used anywhere throughout the execution.
 * No need to get call every time from Properties class.
 */

public class Settings 
{
	
	public static Boolean remote;
	public static String username;
	public static String password;
	public static String timeStampFormat;
	
	// Base URLs
	public static String coinDeskBaseURL;
	public static String randonUserBaseURL;
	public static String genderizeBaseURL;
	
	// Extent report settings
	public static String documentName;
	public static String reportName;
	public static String timeStamp;
	public static String theme;
	public static String pathToSave;
	public static String htmlReportName;
	
	//Database settings
	public static String connectionString;

}
