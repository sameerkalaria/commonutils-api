package com.sameer.commonutils.api.framework.base;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sameer.commonutils.api.framework.common.Asserts;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

/**
 * This class is for handle all REST request type like GET, POST, PUT, DELETE
 */
public class RestFunctions {
	private static final Logger log = LogManager.getLogger(RestFunctions.class);

	/**
	 * GET request call and return response of GET request.
	 * 
	 * @param url    - String. Get request URL. You don't need to specify the path
	 *               as http://localhost:8080/path. In this case it's enough to use
	 *               /path.
	 * @param verify - Boolean. True if want to verify status code of response and
	 *               false if not.
	 * @return - Response of GET request.
	 */
	public static Response get(String url, Boolean verify) {
		Response response = LocalRequestSpecification.getRequest().contentType(ContentType.JSON).when().get(url);

		log.info(response);

		if (verify) {
			Asserts.isRequestSuccess(response.getStatusCode());
		}

		return response;
	}

	/**
	 * GET request call and return response of GET request.
	 * 
	 * @param url - String. GET request URL. You don't need to specify the path as
	 *            http://localhost:8080/path. In this case it's enough to use /path.
	 * @return - Response of GET request with verification of response code.
	 */
	public static Response get(String url) {
		return get(url, true);
	}

	/**
	 * POST request call and return response of POST request. Header will be set as
	 * JSON.
	 * 
	 * @param url    - String. POST request URL. You don't need to specify the path
	 *               as http://localhost:8080/path. In this case it's enough to use
	 *               /path.
	 * @param body   - String. Body of POST request.
	 * @param verify - Boolean. True if want to verify status code of response and
	 *               false if not.
	 * @return - Response of POST request.
	 */
	public static Response post(String url, String body, Boolean verify) {
		Response response = LocalRequestSpecification.getRequest().contentType(ContentType.JSON).body(body).when()
				.post(url);

		log.info(response);

		if (verify) {
			Asserts.isRequestSuccess(response.getStatusCode());
		}

		return response;
	}

	/**
	 * POST request call and return response of POST request. Header will be set as
	 * JSON.
	 * 
	 * @param url  - String. POST request URL. You don't need to specify the path as
	 *             http://localhost:8080/path. In this case it's enough to use
	 *             /path.
	 * @param body - String. Body of POST request.
	 * @return - Response of POST request with verification of response code.
	 */
	public static Response post(String url, String body) {
		return post(url, body, true);
	}

	/**
	 * POST request call and return response of POST request.
	 * 
	 * @param url    - String. POST request URL. You don't need to specify the path
	 *               as http://localhost:8080/path. In this case it's enough to use
	 *               /path.
	 * @param body   - String. Body of POST request.
	 * @param header - Map<String, String>. Header of request.
	 * @param verify - Boolean. True if want to verify status code of response and
	 *               false if not.
	 * @return - Response of POST request.
	 */
	public static Response postWithHeader(String url, String body, Map<String, String> header, Boolean verify) {
		Response response = LocalRequestSpecification.getRequest().contentType(ContentType.JSON).headers(header)
				.body(body).when().post(url);

		log.info(response);

		if (verify) {
			Asserts.isRequestSuccess(response.getStatusCode());
		}

		return response;
	}

	/**
	 * POST request call and return response of POST request.
	 * 
	 * @param url    - String. POST request URL. You don't need to specify the path
	 *               as http://localhost:8080/path. In this case it's enough to use
	 *               /path.
	 * @param body   - String. Body of POST request.
	 * @param header - Map<String, String>. Header of request.
	 * @return - Response of POST request with verification of response code.
	 */
	public static Response postWithHeader(String url, String body, Map<String, String> header) {
		return postWithHeader(url, body, header, true);
	}

	/**
	 * POST request call with authentication and return response of POST request.
	 * Header will be set as JSON.
	 * 
	 * @param url      - String. POST request URL. You don't need to specify the
	 *                 path as http://localhost:8080/path. In this case it's enough
	 *                 to use /path.
	 * @param body     - String. Body of POST request.
	 * @param username - String. Username for authentication
	 * @param password - String. Password
	 * @param verify   - Boolean. True if want to verify status code of response and
	 *                 false if not.
	 * @return - Response of POST request.
	 */
	public static Response postWithBasicAuth(String url, String body, String username, String password,
			Boolean verify) {
		Response response = LocalRequestSpecification.getRequest().auth().preemptive().basic(username, password)
				.contentType(ContentType.JSON).body(body).when().post(url);

		log.info(response);

		if (verify) {
			Asserts.isRequestSuccess(response.getStatusCode());
		}

		return response;
	}

	/**
	 * POST request call with authentication and return response of POST request.
	 * Header will be set as JSON.
	 * 
	 * @param url      - String. POST request URL. You don't need to specify the
	 *                 path as http://localhost:8080/path. In this case it's enough
	 *                 to use /path.
	 * @param body     - String. Body of POST request.
	 * @param username - String. Username for authentication
	 * @param password - String. Password
	 * @return - Response of POST request with verification of response code.
	 */
	public static Response postWithBasicAuth(String url, String body, String username, String password) {
		return postWithBasicAuth(url, body, username, password, true);
	}

	/**
	 * PUT request call and return response of PUT request. Header will be set as
	 * JSON.
	 * 
	 * @param url    - String. PUT request URL. You don't need to specify the path
	 *               as http://localhost:8080/path. In this case it's enough to use
	 *               /path.
	 * @param body   - String. Body of PUT request.
	 * @param verify - Boolean. True if want to verify status code of response and
	 *               false if not.
	 * @return - Response of PUT request.
	 */
	public static Response put(String url, String body, Boolean verify) {
		Response response = LocalRequestSpecification.getRequest().contentType(ContentType.JSON).body(body).when()
				.put(url);

		log.info(response);

		if (verify) {
			Asserts.isRequestSuccess(response.getStatusCode());
		}

		return response;
	}

	/**
	 * PUT request call and return response of PUT request. Header will be set as
	 * JSON.
	 * 
	 * @param url  - String. PUT request URL. You don't need to specify the path as
	 *             http://localhost:8080/path. In this case it's enough to use
	 *             /path.
	 * @param body - String. Body of PUT request.
	 * @return - Response of PUT request with verification of response code.
	 */
	public static Response put(String url, String body) {
		return put(url, body, true);
	}

	/**
	 * PUT request call and return response of PUT request.
	 * 
	 * @param url    - String. PUT request URL. You don't need to specify the path
	 *               as http://localhost:8080/path. In this case it's enough to use
	 *               /path.
	 * @param body   - String. Body of PUT request.
	 * @param header - Map<String, String>. Header of request.
	 * @param verify - Boolean. True if want to verify status code of response and
	 *               false if not.
	 * @return - Response of PUT request.
	 */
	public static Response putWithHeader(String url, String body, Map<String, String> header, Boolean verify) {
		Response response = LocalRequestSpecification.getRequest().contentType(ContentType.JSON).headers(header)
				.body(body).when().put(url);

		log.info(response);

		if (verify) {
			Asserts.isRequestSuccess(response.getStatusCode());
		}

		return response;
	}

	/**
	 * PUT request call and return response of PUT request.
	 * 
	 * @param url    - String. PUT request URL. You don't need to specify the path
	 *               as http://localhost:8080/path. In this case it's enough to use
	 *               /path.
	 * @param body   - String. Body of PUT request.
	 * @param header - Map<String, String>. Header of request.
	 * @return - Response of PUT request with verification of response code.
	 */
	public static Response putWithHeader(String url, String body, Map<String, String> header) {
		return putWithHeader(url, body, header, true);
	}

	/**
	 * PATCH request call and return response of PATCH request. Header will be set
	 * as JSON.
	 * 
	 * @param url    - String. PATCH request URL. You don't need to specify the path
	 *               as http://localhost:8080/path. In this case it's enough to use
	 *               /path.
	 * @param body   - String. Body of PATCH request.
	 * @param verify - Boolean. True if want to verify status code of response and
	 *               false if not.
	 * @return - Response of PUT request.
	 */
	public static Response patch(String url, String body, Boolean verify) {
		Response response = LocalRequestSpecification.getRequest().contentType(ContentType.JSON).body(body).when()
				.patch(url);

		log.info(response);

		if (verify) {
			Asserts.isRequestSuccess(response.getStatusCode());
		}

		return response;
	}

	/**
	 * PATCH request call and return response of PATCH request. Header will be set
	 * as JSON.
	 * 
	 * @param url  - String. PATCH request URL. You don't need to specify the path
	 *             as http://localhost:8080/path. In this case it's enough to use
	 *             /path.
	 * @param body - String. Body of PATCH request.
	 * @return - Response of PATCH request with verification of response code.
	 */
	public static Response patch(String url, String body) {
		return patch(url, body, true);
	}

	/**
	 * PATCH request call and return response of PATCH request.
	 * 
	 * @param url    - String. PATCH request URL. You don't need to specify the path
	 *               as http://localhost:8080/path. In this case it's enough to use
	 *               /path.
	 * @param body   - String. Body of PATCH request.
	 * @param header - Map<String, String>. Header of request.
	 * @param verify - Boolean. True if want to verify status code of response and
	 *               false if not.
	 * @return - Response of PATCH request.
	 */
	public static Response patchWithHeader(String url, String body, Map<String, String> header, Boolean verify) {
		Response response = LocalRequestSpecification.getRequest().contentType(ContentType.JSON).headers(header)
				.body(body).when().patch(url);

		log.info(response);

		if (verify) {
			Asserts.isRequestSuccess(response.getStatusCode());
		}

		return response;
	}

	/**
	 * PATCH request call and return response of PATCH request.
	 * 
	 * @param url    - String. PATCH request URL. You don't need to specify the path
	 *               as http://localhost:8080/path. In this case it's enough to use
	 *               /path.
	 * @param body   - String. Body of PATCH request.
	 * @param header - Map<String, String>. Header of request.
	 * @return - Response of PATCH request with verification of response code.
	 */
	public static Response patchWithHeader(String url, String body, Map<String, String> header) {
		return patchWithHeader(url, body, header, true);
	}

	/**
	 * DELETE request call and return response of DELETE request. Header will be set
	 * as JSON.
	 * 
	 * @param url    - String. DELETE request URL. You don't need to specify the
	 *               path as http://localhost:8080/path. In this case it's enough to
	 *               use /path.
	 * @param body   - String. Body of DELETE request.
	 * @param verify - Boolean. True if want to verify status code of response and
	 *               false if not.
	 * @return - Response of DELETE request.
	 */
	public static Response delete(String url, Boolean verify) {
		Response response = LocalRequestSpecification.getRequest().contentType(ContentType.JSON).when().delete(url);

		log.info(response);

		if (verify) {
			Asserts.isRequestSuccess(response.getStatusCode());
		}

		return response;
	}

	/**
	 * DELETE request call and return response of DELETE request. Header will be set
	 * as JSON.
	 * 
	 * @param url  - String. DELETE request URL. You don't need to specify the path
	 *             as http://localhost:8080/path. In this case it's enough to use
	 *             /path.
	 * @param body - String. Body of DELETE request.
	 * @return - Response of DELETE request with verification of response code.
	 */
	public static Response delete(String url) {
		return delete(url, true);
	}

}
