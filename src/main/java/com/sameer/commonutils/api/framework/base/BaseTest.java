package com.sameer.commonutils.api.framework.base;

import java.io.IOException;
import java.lang.reflect.Method;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.Status;
import com.sameer.commonutils.api.framework.common.ReportUtils;
import com.sameer.commonutils.api.framework.config.ConfigUtils;

import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;

/**
 * This is Base class for all Test case classes. All Test case class should be
 * extend this class. It
 * contains @BeforeSuite, @AfterSuite, @BeforeClass, @AfterClass, @BeforeMethod, @AfterMethod
 * methods. RequestSpecification context, Extent Report context, configuration
 * file reader is setup in this class.
 */
public class BaseTest {
	protected static final Logger log = LogManager.getLogger(BaseTest.class);
	private RequestSpecification request;

	/**
	 * Getting data from property file and store it into Settings. Create extent
	 * report for suite.
	 */
	@BeforeSuite
	public void initializeConfig() throws IOException {
		log.info("Before suite started");

		ConfigUtils.getSettings();
		ReportUtils.createReport();
	}

	/**
	 * It will execute before every @Test method. Create test for that test case and
	 * add it into extent report.
	 * 
	 * @param method - Instance of Method class. It will automatically added from
	 *               calling method by TestNG.
	 */
	@BeforeMethod
	public void createTest(Method method) {
		ReportUtils.createTest(method.getName());
		ReportUtils.addLog(Status.PASS, "Start test");
	}

	/**
	 * It will execute after every @Test method. Add log into extent report based in
	 * test case status like PASS, FAIL or SKIP.
	 * 
	 * @param result - Instance of ITestResult interface. It will automatically
	 *               added by TestNG.
	 * @param method - Instance of Method class. It will automatically added from
	 *               calling method by TestNG.
	 */
	@AfterMethod
	public void addStatus(ITestResult result, Method method) {
		switch (result.getStatus()) {
		case ITestResult.SUCCESS:
			ReportUtils.addLog(Status.PASS, method.getName() + " testcase passed.");
			break;

		case ITestResult.FAILURE:
			ReportUtils.addLog(Status.FAIL, method.getName() + " testcase failed.");
			break;

		case ITestResult.SKIP:
			ReportUtils.addLog(Status.SKIP, method.getName() + " testcase skipped.");
			break;
		}
	}

	/**
	 * It will execute before every class start execution of test cases. It
	 * initialize RequestSpecification context and set into
	 * LocalRequestSpecification.
	 */
	@BeforeClass
	public void setUp() {
		request = RestAssured.given();
		LocalRequestSpecification.setRequest(request);
	}

	/**
	 * It will execute after all the class test cases executed. It will flush the
	 * extent report.
	 */
	@AfterSuite
	public void cleanUp() {
		log.info("After suite started");

		ReportUtils.report.flush();
	}

}
