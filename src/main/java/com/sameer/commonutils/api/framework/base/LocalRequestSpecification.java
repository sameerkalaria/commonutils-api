package com.sameer.commonutils.api.framework.base;

import io.restassured.specification.RequestSpecification;

/**
 * This class is for handle local RequesrSpecification context. It uses
 * ThreadLocal for storing RequesrSpecification context. So, all
 * RequesrSpecification context will be thread safe during parallel execution.
 */
public class LocalRequestSpecification {

	private static ThreadLocal<RequestSpecification> request = new ThreadLocal<>();

	/**
	 * Get Object of RequestSpecification from ThreadLocal. It will thread safe.
	 * 
	 * @return - Object of RequestSpecification.
	 */
	public static RequestSpecification getRequest() {
		return request.get();
	}

	/**
	 * Set object of RequestSpecification into ThreadLocal.
	 * 
	 * @param req - Object of RequestSpecification.
	 */
	public static void setRequest(RequestSpecification req) {
		request.set(req);
	}

	/**
	 * Set base URL into RequestSpecification object.
	 * 
	 * @param url
	 */
	public static void setbaseURL(String url) {
		request.get().baseUri(url);
	}

	/**
	 * Set Base path into RequestSpecification object.
	 * 
	 * @param basepath
	 */
	public static void setbasePath(String basepath) {
		request.get().basePath(basepath);
	}

	/**
	 * Removes the current thread's value for this thread-local variable
	 */
	public static void removeRequest() {
		request.remove();
	}

}
