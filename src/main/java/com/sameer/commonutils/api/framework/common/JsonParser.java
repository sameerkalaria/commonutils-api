package com.sameer.commonutils.api.framework.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.aventstack.extentreports.Status;
import com.google.gson.JsonParseException;

import io.restassured.response.Response;

/**
 * This class is for handle all JSON related operations like convert String to
 * JSON, JSON to Map etc.
 */
public class JsonParser {
	private static final Logger log = LogManager.getLogger(JsonParser.class);
	private static volatile JSONParser jsonParser = new JSONParser();

	public static JSONObject toJSON(String jsonString) throws ParseException {

		try {
			Object object = jsonParser.parse(jsonString);

			if (object instanceof org.json.simple.JSONObject) {
				org.json.simple.JSONObject json = (org.json.simple.JSONObject) object;
				return new JSONObject(json.toJSONString());
			} else {
				log.error("Parsed JSON is not a JSONObject. parced JSON : " + object);
				ReportUtils.addLog(Status.FAIL, "Parsed JSON is not a JSONObject. parced JSON : " + object);
				throw new JsonParseException("Parsed JSON is not a JSONObject. parced JSON : " + object);
			}

		} catch (ParseException e) {
			log.error("Error while parcing json", e);
			ReportUtils.addLog(Status.FAIL, e);
			throw new JsonParseException(e);
		}

	}

	public static JSONObject toJSON(Response response) throws ParseException {

		try {
			Object object = jsonParser.parse(response.getBody().asString());

			if (object instanceof org.json.simple.JSONObject) {
				org.json.simple.JSONObject json = (org.json.simple.JSONObject) object;
				return new JSONObject(json.toJSONString());
			} else {
				log.error("Parsed JSON is not a JSONObject. parced JSON : " + object);
				ReportUtils.addLog(Status.FAIL, "Parsed JSON is not a JSONObject. parced JSON : " + object);
				throw new JsonParseException("Parsed JSON is not a JSONObject. parced JSON : " + object);
			}

		} catch (ParseException e) {
			log.error("Error while parcing json", e);
			ReportUtils.addLog(Status.FAIL, e);
			throw new JsonParseException(e);
		}

	}

	public static JSONObject toJSON_(String jsonString) {
		return new JSONObject(jsonString);
	}

	public static JSONObject toJSON_(Response response) {
		return new JSONObject(response.asString());
	}

	public static Map<String, Object> toMap(JSONObject json) {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			Iterator<String> itr = json.keys();

			while (itr.hasNext()) {
				String key = itr.next();
				Object value = json.get(key);

				if (value instanceof JSONArray) {
					value = toList((JSONArray) value);
				} else if (value instanceof JSONObject) {
					value = toMap((JSONObject) value);
				}
				map.put(key, value);
			}

		} catch (Exception e) {
			log.error("Error while parcing json to Map", e);
			ReportUtils.addLog(Status.FAIL, e);
			throw new JsonParseException(e);
		}

		return map;
	}

	private static List<Object> toList(JSONArray array) {
		List<Object> list = new ArrayList<Object>();
		for (int i = 0; i < array.length(); i++) {
			Object value = array.get(i);
			if (value instanceof JSONArray) {
				value = toList((JSONArray) value);
			}

			else if (value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			list.add(value);
		}
		return list;
	}

}
